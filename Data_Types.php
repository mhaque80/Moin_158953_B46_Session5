<?php

$myBooleanVar='0';

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False <br>';
}


//string example start

$myVar=4848;
$myStr="Hello World! $myVar   <br>";
echo $myStr;
$myStr='Hello World! $myVar   <br>';
echo $myStr;

$heredocVar=<<<BITM

FDSFDSFDSFSFADSAFDSAF
DSFDSAFDSAFDSADDSFSAF $myVar
BITM;

echo $heredocVar;


$nowdocVar=<<<'BITM'

FDSFDSFDSFSFADSAFDSAF
DSFDSAFDSAFDSADDSFSAF $myVar
BITM;

echo $nowdocVar;


//array example here

$myArray=array(13,true,75.25,"Hello",5);

echo "<pre>";

print_r($myArray);

var_dump($myArray);

echo "</pre>";

//array example here




$personAge=array(12.36,"Rahim"=>35,"Karim"=>45,"Jarina"=>18,60.35);

echo $personAge{'Rahim'}."<br>";

print_r($personAge);



$personAge=array("10"=>12.36,"Rahim"=>35,"Karim"=>45,"Jarina"=>18,60.35);

echo $personAge{'Rahim'}."<br>";

print_r($personAge);